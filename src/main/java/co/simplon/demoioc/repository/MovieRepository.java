package co.simplon.demoioc.repository;

import java.util.ArrayList;
import java.util.List;

import co.simplon.demoioc.dao.MovieDAO;
import co.simplon.demoioc.models.Movie;

public class MovieRepository implements MovieDAO {

    private List<Movie> movieList = new ArrayList<>();

    public MovieRepository() {
        Movie alien = new Movie(1, "Aliens");
        Movie backToTheFuture = new Movie(2, "Back to the Future");
        Movie theThing = new Movie(3, "The Thing");

        this.movieList.add(alien);
        this.movieList.add(backToTheFuture);
        this.movieList.add(theThing);
    }

    @Override
    public List<Movie> findAll() {
        return this.movieList;
    }

    @Override
    public Movie findById(int id) {
        for (Movie eachMovie : this.movieList) {
            if (eachMovie.getMovieId() == id) {
                return eachMovie;
            }
        }
        return null;
    }

    @Override
    public Movie save(Movie movie) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'save'");
    }

    @Override
    public void delete(Movie movie) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'delete'");
    }

}
