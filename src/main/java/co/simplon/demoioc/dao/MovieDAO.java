package co.simplon.demoioc.dao;

import java.util.List;

import co.simplon.demoioc.models.Movie;

// DAO vs DTO
public interface MovieDAO {

    public List<Movie> findAll();

    public Movie findById(int i);

    public Movie save(Movie movie);

    public void delete(Movie movie);
}
