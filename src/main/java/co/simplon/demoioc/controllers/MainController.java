package co.simplon.demoioc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.demoioc.models.Movie;
import co.simplon.demoioc.services.MovieService;

import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class MainController {

    // c'est quoi un controller ?
    /*
     * contenir les middlewares, les endpoints (routes)
     */

    private MovieService movieService;

    private MainController(MovieService movieServiceInjected) {
        this.movieService = movieServiceInjected;
    }

    // GET /test -> "ça marche"
    @GetMapping("test")
    public String getTestMessage() {
        System.out.println("coucou");
        return "ça marche";
    }

    // GET /movies liste des films
    @GetMapping("movies")
    public List<Movie> getMovieList() {
        // avant je créais ici la liste des films : couplage fort
        return this.movieService.findAllMovies();
    }

    // movies/search?id=xxx
    @GetMapping("movies/search")
    public Movie getMethodName(@RequestParam int id) {
        return this.movieService.findMovieById(id);
    }


}
