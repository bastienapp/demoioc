package co.simplon.demoioc.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.simplon.demoioc.models.Movie;
import co.simplon.demoioc.repository.MovieRepository;

@Service
public class MovieService {

    private MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepositoryInjected) {
        this.movieRepository = movieRepositoryInjected;
    }

    public List<Movie> findAllMovies() {

        return this.movieRepository.findAll();
    }

    public Movie findMovieById(int id) {

        return this.movieRepository.findById(id);
    }
}
