package co.simplon.demoioc.models;

// lombok : on aura accès à des annotations
// JavaBean : constructeur vide, getter/setter, attributs private, serializable (peut être transformé en texte/json)
public class Movie {

    private int movieId;
    private String title;

    public Movie() {}

    public Movie(int movieId, String title) {
        this.movieId = movieId;
        this.title = title;
    }

    public int getMovieId() {
        return this.movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
